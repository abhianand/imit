const Footer = () => {
    return (
        <footer>
            <div className="container footer_content">
                <div className="footer_list_logo">
                    <div className="footerlist">
                        <div className="footerlist_list">
                            <h4>Job</h4>
                            <ul className="list">
                                <li>
                                    <a href="#">Home</a>
                                </li>
                                <li>
                                    <a href="#">All events</a>
                                </li>
                                <li>
                                    <a href="#">IMIT for Business</a>
                                </li>
                                <li>
                                    <a href="#">Job Openings</a>
                                </li>
                            </ul>
                        </div>
                        <div className="footerlist_list">
                            <h4>Account</h4>
                            <ul className="list">
                                <li>
                                    <a href="#">FAQs</a>
                                </li>
                                <li>
                                    <a href="#">All Venues</a>
                                </li>
                                <li>
                                    <a href="#">User Account</a>
                                </li>
                                <li>
                                    <a href="#">Business Account</a>
                                </li>
                            </ul>
                        </div>
                        <div className="footerlist_list">
                            <h4>IMIT</h4>
                            <ul className="list">
                                <li>
                                    <a href="#">About us</a>
                                </li>
                                <li>
                                    <a href="#">Our Blogs</a>
                                </li>
                                <li>
                                    <a href="#">Press Coverage</a>
                                </li>
                                <li>
                                    <a href="#">Terms & Conditions</a>
                                </li>
                            </ul>
                        </div>
                        <div className="footerlist_list">
                            <h4>Contact</h4>
                            <ul className="list">
                                <li>
                                    <a href="#">Refunds</a>
                                </li>
                                <li>
                                    <a href="#">Contact Us</a>
                                </li>
                                <li>
                                    <a href="#">Privacy Policy</a>
                                </li>
                                <li>
                                    <a href="#">Business Demo</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div className="footerlogo">
                        <h2>IMIT</h2>
                        <p>Subscribe to our newsletter to stay up to date</p>

                        <div className="subscribe_input">
                            <input type="text" className="subscribe" placeholder="Subscribe with your email" />
                            <button type="button">
                                <img src="./images/arowsymbol.png" />
                            </button>

                        </div>
                    </div>
                </div>

                <div className="footer_socialmedia">
                    <div className="secure_text">
                        <h6>SECURED BY STRIPE <br />&copy; 2020 IMIT ALL RIGHTS RESERVED</h6>

                    </div>
                    <div className="socialmedia_icons">
                        <ul className="social_icons">
                            <li>
                                <a href="#">
                                    <img src="./images/fb.png" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="./images/twitter.png" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="./images/you tube.png" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="./images/instagram.png" />
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>

            </div>

        </footer>

    )
}

export default Footer;