import { useSelector } from "react-redux";
import Logo from "./logo";
import MenuItems from "./menuItem";

const headerMenus = [
    {
        title: "Home",
        path: "/",
    },
    {
        title: "Posts",
        path: "/posts"
    },
    {
        title: "About",
        path: "/about"
    },
    {
        title: "Careers",
        path: "/careers"
    },
    {
        title: "News & Updates",
        path: "/news-updates"
    },
    {
        title: "Contact",
        path: "/contact"
    },
    {
        title: "Profile",
        path: "/profile"
    }
]


const Header = () => {

    const notifications = useSelector(store => store.notification)


    return (
        <>
            <header className="container">
                <Logo />
                <div className="nav_icon">
                    <img src="./images/menuicon.png" />
                </div>
                <div className="nav_bar">
                    <ul className="navigation">
                        {
                            headerMenus.map(menu =>
                                <MenuItems title={menu.title} path={menu.path} key={menu.path} />
                            )
                        }
                        <li>
                            <a className="header_button" href="#">{notifications.notificationCount}</a>
                        </li>
                    </ul>
                </div>
            </header>
        </>
    )
}

export default Header;