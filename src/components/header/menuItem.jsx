import { Link } from "react-router-dom";


const MenuItems = ({ path, title }) => {

    return (
        <>
            <li>
                <Link to={path}>{title}</Link>
            </li>
        </>
    )
}

export default MenuItems;