const Logo = () => {
    return (
        <div className="logo">
            <div className="bg_orange"></div>
            <h1>IMIT</h1>
        </div>
    )
}

export default Logo;