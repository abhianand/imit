import { createSlice } from '@reduxjs/toolkit';

const firstValue = {
    notificationCount: 0,
}

const notificationCounter = createSlice({
    name: "notification",
    initialState: firstValue,
    //creating action
    reducers: {
        addNotification: (state, action) => {
            console.log({ state, action });
            state.notificationCount += action.payload || 0;
        }
    }
})

const { addNotification } = notificationCounter.actions

export { addNotification }

export default notificationCounter.reducer;