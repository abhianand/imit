import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom"
import Loader from "../components/loader";

export default function Post() {
    const { postId } = useParams()
    const [post, setPost] = useState({})
    const [showLoader, setShowLoader] = useState(true)

    useEffect(() => {
        getPost()
    }, [])

    async function getPost() {
        try {
            const response = await axios.get(`https://jsonplaceholder.typicode.com/posts/${postId}/sdasd`)
            setPost(response.data)
        } catch (error) {
            alert("Error loading the post")
        } finally {
            setShowLoader(false)
        }

    }

    return (
        <div className="container mx-auto">
            {
                showLoader ?
                    <Loader /> :
                    <div>
                        <h1>{post.title}</h1>
                        <h3>Written by: {post.userId}</h3>
                        <p>{post.body}</p>
                    </div>
            }

        </div>

    )
}