import axios from "axios";
import { useState } from "react";
const Form = () => {
    const [user, setUser] = useState({
        firstName: '',
        secondName: '',
        email: '',
        number: '',
        gender: '',
        dob: '',
        postcode: '',
        location: '',
        password: ''

    })

    const [isSubmitted, setIsSubmitted] = useState(false)
    const [showPassword, setShowPassword] = useState(false)

    async function submitForm(e) {
        e.preventDefault()
        setIsSubmitted(true)
        if (!user.firstName || !user.secondName || !user.email || !checkEmailRegex()) {
            return false
        }
        console.log("Submitting the form");
        try {
            const response = await axios.post(" http://localhost:8080/user", user)
            console.log(response.date);
            // alert("Your form is successfully submitted")
        } catch (error) {
            // alert("Error in submitting")
        }

    }

    const onChangefn = (e) => {
        const { name, value } = e.target;
        setUser({ ...user, [name]: value })
    }

    const checkEmailRegex = () => /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(user.email)

    return (
        <div className="whiteframe">
            <form className="inframe">
                <div>
                    <label htmlFor="firstName" className="form-label">First Name</label>
                    <input type="text" id="firstName" name="firstName" placeholder="Enter your first name" value={user.firstName} className="form-control" onChange={onChangefn} />
                    {(!user.firstName && isSubmitted) && <label className="alert-danger">First name is required</label>}
                </div>
                <div>
                    <label htmlFor="secondName" className="form-label">Second Name</label>
                    <input type="text" id="secondName" name="secondName" placeholder="Enter your second name" value={user.secondName} className="form-control" onChange={onChangefn} />
                    {(!user.secondName && isSubmitted) && <label className="alert-danger">Second name is required</label>}
                </div>
                <div>
                    <label htmlFor="email" className="form-label">Email Address</label>
                    <input type="text" id="email" name="email" placeholder="Enter your email" value={user.email} className="form-control" onChange={onChangefn} />
                    {(!user.email && isSubmitted) && <label className="alert-danger">Email is required</label>}
                    {(user.email && isSubmitted && !checkEmailRegex()) && <label className="alert-danger">Email is not in correct format</label>}
                </div>
                <div>
                    <label htmlFor="number" className="form-label">Phone Number</label>
                    <input type="number" id="number" name="number" placeholder="Enter your personal number" value={user.number} className="form-control" onChange={onChangefn} />
                </div>
                <div>
                    <label htmlFor="location" className="form-label">Location</label>
                    <select id="location" name="location" placeholder="Enter your current location" value={user.location} className="form-select" onChange={onChangefn}>
                        <option value="palakkad">Palakkad</option>
                        <option value="ernakulam">Ernakulam</option>
                        <option value="kozhikode">Kozhikode</option>
                        <option value="wayanad">Wayanad</option>
                    </select>
                </div>
                <div>
                    <label htmlFor="postcode" className="form-label">Post Code</label>
                    <input type="number" id="postcode" name="postcode" placeholder="Enter your postcode" value={user.postcode} className="form-control" onChange={onChangefn} />
                </div>
                <div>
                    <input type="radio" id="gender" name="gender" value={user.gender} className="form-check-input" onChange={onChangefn} />
                    <label htmlFor="gender" className="form-check-label">Female</label>
                    <input type="radio" id="gender" name="gender" value={user.gender} className="form-check-input" onChange={onChangefn} />
                    <label htmlFor="gender" className="form-check-label">Male</label>
                    <input type="radio" id="gender" name="gender" value={user.gender} className="form-check-input" onChange={onChangefn} />
                    <label htmlFor="gender" className="form-check-label">Not interested to disclose</label>
                </div>
                <div>
                    <label htmlFor="dob" className="form-label">Date Of Birth</label>
                    <input type="date" id="dob" name="dob" value={user.dob} className="form-control" onChange={onChangefn} />
                </div>
                <div>
                    <label htmlFor="password" className="form-label">Password</label>
                    <input type={showPassword ? "text" : "password"} id="password" name="password" placeholder="Enter your password" value={user.password} className="form-control" onChange={onChangefn} />
                    <button type="button" onClick={() => { setShowPassword(!showPassword) }} >{showPassword ? "Hide" : "Show"}</button>
                </div>
                <button type="submit" className="btn btn-primary" onClick={submitForm}>Submit</button>
            </form>

        </div>
    )
}
export default Form;