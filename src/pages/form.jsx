import { useEffect, useRef, useState } from "react"

export default function Form() {
    const [user, setUser] = useState({
        userName: "",
        location: ""
    })

    const [displayData, setDisplayData] = useState({
        userName: "",
        location: ""
    })

    const nameRef = useRef(null)

    const [fnSubmitted, setFunctionSubmitted] = useState(false)

    function onChangeFn(e) {
        const { name, value } = e.target
        setUser({ ...user, [name]: value }) // user = {[name]: value}
        setFunctionSubmitted(false)
    }

    function onSubmitFn(e) {
        e.preventDefault();
        if (!user.userName && !user.location) {
            return false
        }
        setFunctionSubmitted(true)
        setDisplayData(user)
        setUser({
            userName: "",
            location: ""
        });
    }

    function editFn() {
        if (!displayData.userName && !displayData.location) {
            return false
        }
        setUser(displayData)
        setDisplayData({
            userName: "",
            location: ""
        })
        setFunctionSubmitted(false);
    }

    useEffect(() => {
        console.log("1. This useEffect with userName")

    }, [user.userName])

    useEffect(() => {
        setTimeout(() => {
            nameRef.current.focus()

        }, 1000);
        console.log("2. This is simply a useEffect")
        return () => {
            console.log("3. This useEffect is unmounted");
        };
    }, [])

    return (
        <>
            <div className="form">
                <form>
                    <div>
                        <label>Name</label> <br />
                        <input type="text" name="userName" onChange={onChangeFn} value={user.userName} ref={nameRef} /> <br />
                    </div>
                    <div>
                        <label>Location</label> <br />
                        <input type="text" name="location" onChange={onChangeFn} value={user.location} />
                    </div>
                    <button type="submit" onClick={onSubmitFn} >Submit</button>
                </form>
                {fnSubmitted && <p>Hey I&apos;m {displayData.userName} and I&apos;m from {displayData.location}</p>}
                <button onClick={editFn}>Edit</button>
            </div>
        </>
    )
}
