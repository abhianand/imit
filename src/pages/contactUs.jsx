import axios from "axios"
import { useState } from "react"

export default function ContactUs() {
    const [user, setUser] = useState({
        email: '',
        username: '',
        location: '',
        dob: ''
    })

    async function submitForm(e) {
        try {
            e.preventDefault() // prevent page reload
            const response = await axios.post("http://localhost:8080/user", user)
            console.log(response.data);
            alert("User details saved successfully")
        } catch (error) {
            alert("Error saving user details")
        }

    }

    const onChangeFn = (e) => {
        const { name, value } = e.target;
        setUser({ ...user, [name]: value })
    }

    return (
        <section className="grey_container">
            <div className="container subgrey_whatare">
                <h3>Contact Us</h3>
                <form>
                    <div>
                        <label htmlFor="email" className="form-label">Email address</label>
                        <input type="email" id="email" name="email" value={user.email}
                            placeholder="Enter email"
                            className="form-control"
                            onChange={onChangeFn}
                        />
                    </div>
                    <div>
                        <label htmlFor="name" className="form-label">name</label>
                        <input type="text"
                            name="username"
                            value={user.username} id="name"
                            placeholder="Enter your full name"
                            className="form-control"
                            onChange={onChangeFn} />
                    </div>
                    <div>
                        <label htmlFor="location" className="form-label">Location</label>
                        <select className="form-select" name="location" value={user.location} id="location"
                            onChange={onChangeFn}>
                            <option value=""></option>
                            <option value="Kochi">Kochi</option>
                            <option value="Bangalore">Bangalore</option>
                            <option value="Chennai">Outer Chennai</option>
                            <option value="Delhi">Delhi</option>
                            <option value="Mumbai">Mumbai</option>
                        </select>
                    </div>
                    <div>
                        <label htmlFor="dateofbirth" className="form-label">Distance from kochi</label>
                        <input className="form-control"
                            name="dob"
                            value={user.dob}
                            type="date"
                            onChange={onChangeFn}
                            min="2000-01-01"
                            max="2011-12-31"
                        />
                    </div>
                    <button type="submit" className="btn btn-primary" onClick={submitForm}>Submit</button>
                </form>
            </div>
        </section>
    )
}