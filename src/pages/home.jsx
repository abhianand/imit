import { useDispatch } from "react-redux";
import { addNotification } from "../redux/notificationReducer";

const Home = () => {

    const dispatch = useDispatch()

    function addNotificationFn(n) {
        dispatch(addNotification(n))
    }

    return (
        <>
            <section className="carosel_image">
                <img src="images/caroselimage.png" className="carosel_image_style" />

                <img src="./images/mobilecarosel.jpg" className="mob_carosel_image" />

                <button onClick={() => addNotificationFn()}  >Add 0 notification</button>
                <button onClick={() => addNotificationFn(1)}  >Add 1 notification</button>
                <button onClick={() => addNotificationFn(2)}  >Add 2 notification</button>

                <div className="captions">
                    <h1>IMIT</h1>
                    <h3>IRINJALAKKUDA</h3>
                    <div className="org_bar"></div>
                    <p>International Media & <br /> Information Technology Park</p>
                </div>
            </section>
            <section className="container watsnew_content">
                <h2>What's New ? </h2>
                <p>Evgenis UK Ltd. is one of the prestigious fast developing import export firm, based in London, United Kingdom. Our mission at Evgenis UK is to provide complete import/export services including purchase contracts, shipping, and delivery scheduling.</p>


            </section>



        </>
    )
}

export default Home;