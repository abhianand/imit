import axios from "axios"
import { useEffect, useState } from "react"

export default function About() {
    const [respData, setResponseData] = useState([])

    useEffect(() => {
        makeRequest()
    }, [])

    const makeRequest = async () => {
        const response = await axios.get("https://jsonplaceholder.typicode.com/posts")
        console.log(response);
        setResponseData(response.data)
    }

    return (
        <>
            <section className="grey_container">
                <div className="container subgrey_whatare">
                    <div className="grey_bg_text">
                        <h2>What</h2>
                        <h5>are you looking for?</h5>
                        <small>We provide IT related solutions for your business or your career</small>
                    </div>
                    <div className="grey_bg_box">
                        <div className="text_box image">
                            <div className="blue_layer"></div>
                            <h4 className="text_layaer">Do you need a</h4>
                            <h2 className="text_layaer">Space ?
                                <small>Lorem ipsum doler sit umet</small>
                            </h2>


                        </div>

                        <div className="text_box indigo">
                            <h4>Do you need a</h4>
                            <h2>Job ?</h2>
                            <small>Lorem ipsum doler sit umet</small>
                        </div>
                        <div className="text_box orange">
                            <h4>Do you need any kind of</h4>
                            <h2>Business Help ?</h2>
                            <small>Lorem ipsum doler sit umet</small>
                        </div>
                        <div>
                            {
                                respData.map(rdata => {
                                    return (
                                        <div className="text_box" key={rdata.id}>
                                            <h4 className="text_layaer">{rdata.title}</h4>
                                            <p>{rdata.body}</p>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}