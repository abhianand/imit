import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const Posts = () => {

    const [posts, setPosts] = useState([]);
    //to call the function getPost
    useEffect(() => {
        getPosts();

        axios.get('https://jsonplaceholder.typicode.com/posts').then((response) => {
            setPosts(response.data)
        }).catch((error) => {
            console.log(error);
        }).finally(() => {

        })

    }, [])

    //call api to backend to get posts
    const getPosts = async () => {
        // try {
        //     const response = await axios.get('https://jsonplaceholder.typicode.com/posttt')
        //     setPosts(response.data);
        // } catch (error) {
        //     console.log(error);
        //     alert('Error retrieving data!!!')
        // } finally {
        //     setPosts([])
        // }

    }



    return (
        <>
            <section className="container watsnew_content">
                {
                    posts.map(post => {
                        return (
                            <div key={post.id}>
                                <div style={{ "borderTop": "10px solid #fff;" }}>
                                    <div >
                                        <h1>{post.title}</h1>
                                        <h3>Written by: {post.userId}</h3>
                                    </div>
                                    <p>{post.body}</p>
                                    <Link to={`/post/${post.id}`}>Open post</Link>
                                </div>
                            </div>
                        )
                    })
                }
            </section>
        </>
    )
}

export default Posts;