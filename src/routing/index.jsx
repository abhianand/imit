import { Route, Routes } from "react-router-dom";
import Footer from "../components/footer";
import Header from "../components/header/header";
import { Suspense, lazy } from "react";
import Form from "../pages/form";

const Home = lazy(() => import('../pages/home'))
const About = lazy(() => import('../pages/about'))
const ContactUs = lazy(() => import('../pages/contactUs'))
const Posts = lazy(() => import('../pages/posts'))
const Post = lazy(() => import('../pages/post'))
const SingUp = lazy(() => import('../pages/signUp'))

const Routing = () => {
    return (
        <>
            <Header />
            <Suspense fallback="Loading.." >

                <Routes>
                    <Route Component={Home} path="/" />
                    <Route Component={About} path="/about" />
                    <Route Component={ContactUs} path="/contact" />
                    <Route Component={Posts} path="/posts" />
                    <Route Component={Post} path="/post/:postId" />
                    <Route Component={SingUp} path="/signUp" />
                    <Route Component={Form} path="/form" />
                </Routes>
            </Suspense>
            <Footer />
        </>
    )
}

export default Routing;