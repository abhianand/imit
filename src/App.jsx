import { Provider } from 'react-redux'
import './App.css'
import Routing from './routing'
import './web.css'
import store from './redux'

function App() {
  return (
    <>
      <Provider store={store}>
        <Routing />
      </Provider>
      {/* </Suspense> */}
    </>
  )
}

export default App
